<?php

namespace Serenata\Common;

use JsonSerializable;

/**
 * Describes a range in a document.
 *
 * This is a value object and immutable.
 *
 * @see https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#range
 */
final class Range implements JsonSerializable
{
    /**
     * The (inclusive, starting from zero) start position.
     *
     * @var Position
     */
    private $start;

    /**
    * The (exclusive, starting from zero) end position.
    *
     * @var Position
     */
    private $end;

    /**
     * @param Position $start
     * @param Position $end
     */
    public function __construct(Position $start, Position $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return Position
     */
    public function getStart(): Position
    {
        return $this->start;
    }

    /**
     * @return Position
     */
    public function getEnd(): Position
    {
        return $this->end;
    }

    /**
     * @param Position $position
     *
     * @return bool
     */
    public function contains(Position $position): bool
    {
        return $position->liesAfterOrOn($this->getStart()) && $position->liesBefore($this->getEnd());
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'start' => $this->getStart(),
            'end'   => $this->getEnd()
        ];
    }
}
